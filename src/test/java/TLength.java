import static org.junit.Assert.*;

import com.bank.banking.banking.BusinessLogic.CodeGenerator;
import org.junit.Test;

public class TLength {

	@Test
	public void test() throws Exception {
		CodeGenerator gen = new CodeGenerator();
		
		//runs 20 times
		for(int j = 0; j < 20; j++) {
			//test itself
			for(int i = 0; i < 100; i++) {
				String test = gen.genCode();
				
				if(test.length() != 6)
					throw new Exception("Not Proper Length");
			}
		}
	}

}
