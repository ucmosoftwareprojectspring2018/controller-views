package com.bank.banking.banking;

import com.bank.banking.banking.controller.PinRepository;
import com.bank.banking.banking.controller.PrimaryController;
import com.bank.banking.banking.model.Pin;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankingApplicationTests {

	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

	private MockMvc mockMvc;

	@MockBean
	private Pin pin;

	@Autowired
	private PrimaryController primaryController;

	@MockBean
	private PinRepository pinRepository;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(primaryController).apply(documentationConfiguration(this.restDocumentation))
				.build();
		pin = new Pin();
		pin.setId(Long.parseLong("1"));
		pin.setAccount("account");
		pin.setPin("pin");
		pin.setCreateTimestamp(claimModificationTime());
		pin.setIp("ip");
		pin.setUser("user");
		pin.setExTime(claimModificationTime());
		pin.setClaimTime(claimModificationTime());
		pin.setClaimIp("claimIp");
		pin.setClaimUser("claimUser");
	}

	@Test
	public void contextLoads() {
	}

	private Timestamp claimModificationTime () {
		Calendar cal = Calendar.getInstance();
		Date utilDate = new Date();
		cal.setTime(utilDate);
		cal.set(Calendar.MILLISECOND, 0);

		return new Timestamp((cal.getTimeInMillis()));
	}

}
