package com.bank.banking.banking.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bank.banking.banking.BusinessLogic.CodeGenerator;
import com.bank.banking.banking.BusinessLogic.ICodeGen;
import com.bank.banking.banking.errors.RESTErrors;
import com.bank.banking.banking.model.Pin;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.hibernate.jpa.criteria.expression.function.CurrentTimestampFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import sun.util.calendar.BaseCalendar;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Scanner;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//Primary controller for the application
@Controller
public class PrimaryController {
    @Autowired
    private PinRepository pinRepository;
    private static final Logger logger = LoggerFactory.getLogger(PrimaryController.class);
    //logger for serverside issue tracking
    //this is the default handler, ie: all the none excepted REST request

    //TODO: Switch time formate for data to YYYY-MM-DD HH:MM:SS
    //TODO: Figure out how to REST DOC

    @RequestMapping
    public @ResponseBody ResponseEntity catchAll(HttpServletRequest request){
        String ipAddress = request.getRemoteAddr();
        String requestType = request.getMethod();
        String requestPath = request.getServletPath();
        Timestamp timeEntered = new Timestamp(System.currentTimeMillis());

        //build error object
        RESTErrors caught = new RESTErrors(ipAddress, requestType, requestPath, timeEntered);

        logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                timeEntered, ipAddress, request.getMethod(), request.getServletPath(),"Rejected: Invalid Request for " + ipAddress + ".");
        //what you request
        //return http
        //return appropriate
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }
    @PostMapping (path="/create", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    //@RequestMapping(value="/create", method=RequestMethod.POST)
    public @ResponseBody ResponseEntity addPin(HttpServletRequest hsr) { //@RequestParam String user
        if ((hsr.getParameter("account") == null) || (hsr.getParameter("account") == "")){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/create", "Rejected: Invalid or unset parameters for account.");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("[{\"msg\": \"Error: Need parameter for account\"}]");
        }
        if ((hsr.getParameter("user") == null) || (hsr.getParameter("user") == "")){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/create", "Rejected: Invalid or unset parameter for user.");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("[{\"account\":\"" + hsr.getParameter("account") + "\", \"msg\": \"Error: Need parameters for user\"}]");
        }
        Integer offset = 48;
        if ((hsr.getParameter("expoffset") != null) || (hsr.getParameter("expoffset") == "")){
            offset = Integer.parseInt(hsr.getParameter("expoffset"));
        }
        Pin pin = new Pin();
        //create pin and perform the mod 10 check
            Pin checkPin = pinRepository.checkForValidPin(hsr.getParameter("account"), new Timestamp(System.currentTimeMillis()));
            if (checkPin != null) {
                logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                        pin.getCreateTimestamp(), pin.getIp(), "POST", "/create", "Rejected: PIN exists for " + pin.getAccount() + ".");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("[{\"account\":\"" +checkPin.getAccount()+"\", \"msg\":\"Error: Pin already exists for account\"}]");
            }
        ICodeGen codeGen = new CodeGenerator();
            while (true) {
                String uniquePin = codeGen.genCode();
                checkPin = pinRepository.findPin(uniquePin);
                if (checkPin == null) {
                    pin.setPin(uniquePin);
                    break;
                }
            }
        //confirmed pin was unique and set it
        pin.setAccount(hsr.getParameter("account"));
        pin.setUser(hsr.getParameter("user"));
        pin.setIp(hsr.getRemoteAddr());
        long time = System.currentTimeMillis();
        java.sql.Timestamp ts1 = new Timestamp(time);
        java.sql.Timestamp ts2 = new Timestamp(time + (24 * 60 * 60 * 1000 * offset));
        pin.setCreateTimestamp(ts1);
        pin.setExTime(ts2);
        pinRepository.save(pin);
        logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                pin.getCreateTimestamp(), pin.getIp(), "POST", "/create", "Accepted: PIN created for " + pin.getAccount() + ".");
        return ResponseEntity.status(HttpStatus.CREATED).body("[{\"account\":\"" + pin.getAccount() + "\", \"pin\":\"" + pin.getPin() + "\"}]");
    }

    @PostMapping(path="/claim", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody ResponseEntity claimPin(HttpServletRequest hsr){
        Pin pin = pinRepository.findPin(hsr.getParameter("pin"));
        Timestamp t = new Timestamp(System.currentTimeMillis());
        if ((hsr.getParameter("pin") == null) || (hsr.getParameter("pin") == "")){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/claim", "Rejected: Invalid or unset parameters for pin.");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("[{\"msg\": \"Error: Need parameter for pin\"}]");
        }

        if (pin == null){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/claim", "Failed: Invalid pin supplied.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("[{\"msg\": \"Error: Pin not found\"}]");
        }
        String account = pin.getAccount();
        pin = pinRepository.checkForValidPin(account,new Timestamp(System.currentTimeMillis()));
        if (pin == null){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/claim", "Failed: Pin is Expired.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("[{\"account\":\"" + account + "\",\"msg\": \"Error: Pin expired\"}]");
        }
        String ipAddress = hsr.getRemoteAddr();
        //check if pin is not expired, then use it if it's available
        Timestamp time = new Timestamp(System.currentTimeMillis());
        pin.setClaimTime(new Timestamp(System.currentTimeMillis()));
        pin.setClaimIp(hsr.getRemoteAddr());
        pin.setClaimUser(hsr.getParameter("user"));
        pinRepository.save(pin);
        logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                pin.getClaimTime(), pin.getIp(), "POST", "/claim", "Accepted: PIN claimed for " + pin.getAccount() + ".");
        return ResponseEntity.status(HttpStatus.OK).body("[{\"account\":\"" + pin.getAccount() + "\"}]");
    }

    @PostMapping (path="/expire", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody ResponseEntity expirePin(HttpServletRequest hsr){
        //check for pin and delete it if found
        if((hsr.getParameter("pin") == null) || (hsr.getParameter("pin") == "")){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/expire", "Rejected: Invalid parameter for pin.");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("[{\"msg\": \"Error: Need parameter for pin\"}]");
        }
        Pin pin = pinRepository.findPin(hsr.getParameter("pin"));
        if (pin == null){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/expire", "Failed: Pin not found.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("[{\"msg\": \"Error: Pin not found\"}]");
        }
        pin.setExTime(new Timestamp(System.currentTimeMillis()));
        pinRepository.save(pin);
        logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                pin.getExTime(), pin.getIp(), "POST", "/expire", "Accepted: PIN expired for " + pin.getAccount() + ".");
        return ResponseEntity.status(HttpStatus.OK).body("[{\"account\":"+ hsr.getParameter("account")+",\"msg\":\"Expired pin\"}]");
    }

    @PostMapping (path="/get", produces = "application/json")
    public @ResponseBody ResponseEntity getPin(HttpServletRequest hsr){
        if ((hsr.getParameter("account") == null) || (hsr.getParameter("account") == "")){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/get", "Rejected: Invalid or unset parameters for account.");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("[{\"msg\": \"Error: Need parameter for account\"}]");
        }
        Pin pin = pinRepository.checkForValidPin(hsr.getParameter("account"), new Timestamp(System.currentTimeMillis()));
        if (pin == null){
            logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                    new Timestamp(System.currentTimeMillis()) , hsr.getRemoteAddr(), "POST", "/get", "Failed: No valid pin for account.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("[{\"account\":\"" + hsr.getParameter("account") + "\",\"msg\": \"Error: Pin not found\"}]");
        }
        logger.info("Time:{},IP:{},RequestType:{},RequestPath:{},ActionTaken:{}",
                pin.getCreateTimestamp(), pin.getIp(), "POST", "/get", "Accepted: PIN fetched for account " + pin.getAccount() + ".");
        return ResponseEntity.status(HttpStatus.CREATED).body("[{\"account\":\"" + pin.getAccount() + "\", \"pin\":\"" + pin.getPin() + "\",\"create_ip\":\"" + pin.getIp()+ "\",\"create_user\":\"" + pin.getUser() + "\",\"create_time\":\"" + pin.getCreateTimestamp() + "\",\"expire_time\":\"" + pin.getExTime() + "\"}]");
    }

    @PostMapping (path="/list")
    public @ResponseBody Iterable<Pin> getAllPins(){
        return pinRepository.findAll();
    }
}