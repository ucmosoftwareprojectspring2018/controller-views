package com.bank.banking.banking.controller;

import com.bank.banking.banking.model.Pin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PinRepository extends CrudRepository<Pin, Long> {
    @Query(value = "Select p from Pin p where p.account = ?1 and p.exTime >= ?2 and p.clTime is null")
    Pin checkForValidPin(String account, Timestamp now);

    @Query(value = "select p from Pin p where p.pin = ?1")
    Pin findPin(String pin);
}