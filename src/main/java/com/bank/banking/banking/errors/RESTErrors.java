package com.bank.banking.banking.errors;

import java.sql.Timestamp;

public class RESTErrors {
    private String ipAddress, requestType, requestPath, errorMessage;
    private Timestamp timeEntered;

    public RESTErrors(String ipAddress, String requestType, String requestPath, Timestamp timeEntered){
        this.ipAddress = ipAddress;
        this.requestType = requestType;
        this.requestPath = requestPath;
        this.timeEntered = timeEntered;
    }

    public String errorType(){
        this.errorMessage = String.format("Type: %s, Path: %s", requestType, requestPath);
        return errorMessage;
    }
}
