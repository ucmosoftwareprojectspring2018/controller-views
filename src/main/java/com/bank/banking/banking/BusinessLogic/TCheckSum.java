package com.bank.banking.banking.BusinessLogic;

//import static org.junit.Assert.*;

import com.bank.banking.banking.BusinessLogic.CodeGenerator;
//import org.junit.Test;

public class TCheckSum {

	//@Test
	public void test() throws Exception {
		//variables
		int checkElement;
		int sampleArr[] = new int[6];
		String sampleCode;
		CodeGenerator gen = new CodeGenerator();
		
		//runs sample 20 times
		for(int j = 0; j < 20; j++) {
			
			//get code and mark last element
			sampleCode = gen.genCode();
		
			//transform string into array
			for(int i = 0; i < sampleCode.length(); i++) {
				sampleArr[i] = Integer.valueOf(sampleCode.charAt(i)) -48;
			}
		
			//log last element
			checkElement = sampleArr[sampleArr.length - 1];
		
			//compute luhnCode
			for (int i = 4; i >= 0; i-= 2) {
				sampleArr[i] = sampleArr[i] * 2;
				//fix numbers larger than 10
				if(sampleArr[i] > 9)
					sampleArr[i] -= 9;
			}
		
			//sum numbers
			int sum = 0;
			for(int i = 0; i < sampleArr.length - 1; i++) {
				sum += sampleArr[i];
			}
		
			if(checkElement != (sum * 9 % 10))
				throw new Exception("Invalid");
		}
	}

}