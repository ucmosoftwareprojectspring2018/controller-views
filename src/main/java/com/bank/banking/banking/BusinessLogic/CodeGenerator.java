package com.bank.banking.banking.BusinessLogic;

import java.util.Random;


public class CodeGenerator implements ICodeGen {
	Random rndNum = new Random(System.currentTimeMillis());
	public CodeGenerator(){
		
	}

	public String genCode() {
		//these modular variables make it easy to change how we want the code generated
		int sizeOfCode = 6;
		int lastElementI = sizeOfCode - 1;
		int lastElement;
		int startCode[] = new int[sizeOfCode];
		int luhnCode[] = new int[sizeOfCode];
		
		//create random five digits to prime mod 10 algorithm
		for(int i = 0; i < lastElementI; i++) {
			startCode[i] = rndNum.nextInt(9);
		}
		
		//Prime Luhn Alogrithmn (double every other number of the string) starting second to last and moving on
		luhnCode = startCode.clone();
		for (int i = lastElementI - 1; i >= 0; i-= 2) {
			luhnCode[i] = luhnCode[i] * 2;
			//fix numbers larger than 10
			if(luhnCode[i] > 9)
				luhnCode[i] -= 9;
		}
		
		//sum digits in array to get info for last digit
		int sum = 0;
		for(int i = 0; i < luhnCode.length - 1; i++) {
			sum += luhnCode[i];
		}
		
		//last element
		lastElement = (sum * 9) % 10;
		startCode[lastElementI] = lastElement;
		
		//convert array into whole string
		String formattedCode = "";
		for(int i = 0; i < startCode.length; i++) {
			formattedCode = formattedCode + String.valueOf(startCode[i]);
		}
		
		return formattedCode;
		
	}
	

	public static void main(String[] args) {
		CodeGenerator bob = new CodeGenerator();
		for(int i = 0; i < 20; i++)
		System.out.println(bob.genCode());

	}

}
