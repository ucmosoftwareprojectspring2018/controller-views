package com.bank.banking.banking.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table
public class Pin {
//TODO check oid, change it to id
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long oid;

    @NotNull
    @Column(name = "account", nullable = false)
    private String account;

    @NotNull
    @Column(name = "pin", nullable = false)
    private String pin;

    @NotNull
    @Column(name = "create_timestamp", nullable = false)
    private Timestamp crTime;

    @NotNull
    @Column(name = "create_ip", nullable = false)
    private String ip;

    @NotNull
    @Column(name = "create_user", nullable = false)
    private String user;

    @NotNull
    @Column(name = "expire_timestamp", nullable = false)
    private Timestamp exTime;

    @Column(name = "claim_timestamp")
    private Timestamp clTime;

    @Column(name = "claim_user")
    private String clUser;

    @Column(name = "claim_ip")
    private String clIp;

    public Long getId() {
        return oid;
    }
    public void setId(Long oid) {
        this.oid = oid;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getPin() {
        return pin;
    }
    public void setPin(String pin) {
        this.pin = pin;
    }
    public Timestamp getCreateTimestamp() {
        return crTime;
    }
    public void setCreateTimestamp(Timestamp crTime) {
        this.crTime = crTime;
    }
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public Timestamp getExTime() {
        return exTime;
    }
    public void setExTime(Timestamp exTime) {
        this.exTime = exTime;
    }
    public Timestamp getClaimTime() {
        return clTime;
    }
    public void setClaimTime(Timestamp clTime) {
        this.clTime = clTime;
    }
    public String getClaimUser() {
        return clUser;
    }
    public void setClaimUser(String clUser) {
        this.clUser = clUser;
    }
    public String getClaimIp() {
        return clIp;
    }
    public void setClaimIp(String clIp) {
        this.clIp = clIp;
    }
}
